﻿using UnityEngine;
using DG.Tweening;

public class EndingPerson : MonoBehaviour
{
    [SerializeField] private Config config;
    [SerializeField] private AudioSource sourse;
    [SerializeField] private AudioClip[] happyClips;

    void OnEnable()
    {
        Invoke(nameof(BeHappy), Random.Range(config.showHappyPeopleEndingAfter.x, config.showHappyPeopleEndingAfter.y));
    }

    private void BeHappy()
    {
        float randRepeat = Random.Range(3, 4);
        InvokeRepeating(nameof(PlayHappySound), randRepeat, randRepeat);
        transform.DOShakeScale(3, 0.2f).SetDelay(randRepeat).SetLoops(-1);
        transform.DOShakeRotation(3, 15).SetDelay(randRepeat).SetLoops(-1);
    }

    private void PlayHappySound()
    {
        sourse.PlayOneShot(happyClips[Random.Range(0, happyClips.Length)]);
    }


}
