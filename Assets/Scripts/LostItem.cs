﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LostItem : MonoBehaviour
{
    public Sprite textSpr;
    [HideInInspector] public bool isOnWindow;
    private float timePassed = 0;

    private AudioSource main, ghor;

    void Start()
    {
        main = GetComponents<AudioSource>()[0];
        ghor = GetComponents<AudioSource>()[1];
    }

    void Update()
    {
        timePassed += Time.deltaTime;
    }

    public void PlayMainSound()
    {
        main.Play();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        ghor.pitch = Random.Range(0.9f, 1.1f);
        ghor.panStereo = (Random.Range(0, 2) - 1) * Random.Range(0.1f, 0.3f);
        if (ghor.clip != null && timePassed > 0.15f)
        {
            ghor.PlayOneShot(ghor.clip);
            timePassed = 0;
        }
    }
}
