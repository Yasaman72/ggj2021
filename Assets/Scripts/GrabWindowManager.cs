﻿using UnityEngine;

public class GrabWindowManager : MonoBehaviour
{
    public delegate void OnItemOnWindowStateChanged(bool isOnWindow);
    public static event OnItemOnWindowStateChanged itemOnWindowStateChanged;

    public static event genericDelegate openningShop;


    [SerializeField] private ItemGenerator itemGenerator;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("LostItem"))
        {
            if (!other.GetComponent<DragHandler>().isDraging) return;
            other.GetComponent<LostItem>().isOnWindow = true;
            itemOnWindowStateChanged?.Invoke(true);
        }

        if (other.gameObject.name.Contains("Key"))
        {
            other.gameObject.GetComponent<DragHandler>().OnDragOnWindow += () =>
                {
                    openningShop?.Invoke();
                    itemGenerator.OnOpenedOffice();
                    Destroy(other.gameObject);
                };
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("LostItem"))
        {
            other.GetComponent<LostItem>().isOnWindow = false;
            itemOnWindowStateChanged?.Invoke(false);
        }

        if (other.gameObject.name.Contains("Key"))
        {
            other.gameObject.GetComponent<DragHandler>().OnDragOnWindow = null;
        }
    }
}
