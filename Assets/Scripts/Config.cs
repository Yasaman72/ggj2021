﻿using UnityEngine;

[CreateAssetMenu(fileName = "game config", menuName = "config")]
public class Config : ScriptableObject
{
    public Vector2 gravityRandomRange = Vector2.one;
    public Vector2 scaleRandomRange = Vector2.one;
    public Vector2 newItemInterval = Vector2.one;
    public int initialGameItemsCound;
    public int maxItemInScreen;
    public Vector2 waitBeforeNewLoser;
    [Space]
    public float timeBeforeMostasal1;
    public float timeBeforeGhor2;
    public float timeBeforeAnger3;
    [Space]
    public int loseChance;
    public float restartGameAfter;
    public Vector2 showHappyPeopleEndingAfter;
}
