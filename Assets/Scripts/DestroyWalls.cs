﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWalls : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!collision.CompareTag("Wall"))
        {
            ItemGenerator.RemoveObjectFromList(collision.gameObject);
        }
    }
}
