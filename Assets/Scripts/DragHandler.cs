﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public delegate void onDragOnWindow();
    public onDragOnWindow OnDragOnWindow;
    public bool isDraging;

    public static GameObject DraggedInstance;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private LostItem lostItem;

    Vector3 _startPosition;
    Vector3 _offsetToMouse;
    float _zDistanceToCamera;
    float _originalGravityScale;

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        _originalGravityScale = rb.gravityScale;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        isDraging = true;
        GameManager.currentDragItem = this;
        rb.bodyType = RigidbodyType2D.Kinematic;
        rb.velocity = Vector2.zero;
        //rb.gravityScale = 0;
        DraggedInstance = gameObject;
        _startPosition = transform.position;
        _zDistanceToCamera = Mathf.Abs(_startPosition.z - Camera.main.transform.position.z);

        _offsetToMouse = _startPosition - Camera.main.ScreenToWorldPoint(
            new Vector3(Input.mousePosition.x, Input.mousePosition.y, _zDistanceToCamera)
        );

        transform.GetComponent<LostItem>().PlayMainSound();
        if (transform.GetComponent<AudioSource>().volume != 0.99f)
        {
            SoundManager.instance.PlayGhorsounds();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (Input.touchCount > 1) return;
        if (!isDraging) return;

        transform.position = Camera.main.ScreenToWorldPoint(
            new Vector3(Input.mousePosition.x, Input.mousePosition.y, _zDistanceToCamera)
            ) + _offsetToMouse;
    }

    public void OnEndDrag(PointerEventData eventData = null)
    {
        GameManager.currentDragItem = null;
        isDraging = false;

        // check if this is what customer wants!
        if (lostItem.isOnWindow)
        {
            if (LoserGenerator.currentLostItem != null)
            {
                if (LoserGenerator.currentLostItem.name == gameObject.name)
                {
                    ItemGenerator.OnGaveAnObjedctBack(gameObject);
                    return;
                }
            }

            OnDragOnWindow?.Invoke();
            OnDragOnWindow = null;
        }

        rb.bodyType = RigidbodyType2D.Dynamic;
        //rb.gravityScale = 1;
        DraggedInstance = null;
        _offsetToMouse = Vector3.zero;
    }

}