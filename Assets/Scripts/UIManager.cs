﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static genericDelegate OnFoundAllItemsOnce;

    [Header("game logotype")]
    [SerializeField] private SpriteRenderer logotype;

    [Header("Score")]
    [SerializeField] private GameObject recordPlate;
    [SerializeField] private TextMeshPro scoreTxt;

    [Header("game Over")]
    [SerializeField] private Config config;
    [SerializeField] private GameObject loseIndicatorUI;
    [SerializeField] private Transform loseHolder;
    private List<GameObject> lostsUI = new List<GameObject>();

    [Header("Progression")]
    [SerializeField] private ItemsDB itemDB;
    [SerializeField] private Transform progressionHodler;
    [SerializeField] private GameObject progressionCheckBox;
    [SerializeField] private Sprite[] progressionCheckSprs;

    private List<GameObject> progressionFill = new List<GameObject>();
    private List<Sprite> foundSprite = new List<Sprite>();

    private bool firstTime = true;

    private void OnEnable()
    {
        recordPlate.SetActive(false);

        for (int i = 0; i < config.loseChance; i++)
        {
            lostsUI.Add(Instantiate(loseIndicatorUI, loseHolder).transform.GetChild(0).gameObject);
        }

        for (int i = 0; i < itemDB.lostItems.Length; i++)
        {
            GameObject obj = Instantiate(progressionCheckBox, progressionHodler);

            GameObject fillObj = obj.transform.GetChild(0).gameObject;
            fillObj.SetActive(false);
            progressionFill.Add(fillObj);

            obj.GetComponent<Image>().sprite = progressionCheckSprs[Random.Range(0, progressionCheckSprs.Length)];
        }

        GameManager.OnLoseChance += OnLostChance;
        ItemGenerator.OnOpennedKerkere += OnShopOpenned;
        LoserGenerator.OnGameOver += Showscore;
    }

    private void OnDisable()
    {
        GameManager.OnLoseChance -= OnLostChance;
        LoserGenerator.OnGameOver -= Showscore;
    }

    private void OnLostChance(int currentAmount)
    {
        for (int i = 0; i < lostsUI.Count; i++)
        {
            lostsUI[i].SetActive(i < currentAmount);
        }
    }

    public void OnGaveNewItem(GameObject gainedItem)
    {
        if (!foundSprite.Contains(gainedItem.GetComponent<SpriteRenderer>().sprite))
        {
            foundSprite.Add(gainedItem.GetComponent<SpriteRenderer>().sprite);

            for (int i = 0; i < foundSprite.Count; i++)
            {
                progressionFill[i].SetActive(true);
            }
        }

        if (foundSprite.Count >= itemDB.lostItems.Length)
        {
            OnFoundAllItemsOnce?.Invoke();
        }
    }

    private void Showscore()
    {
        recordPlate.SetActive(true);
        scoreTxt.text = GameManager.currentScore.ToString();
        Debug.Log("score " + GameManager.currentScore.ToString());
    }

    private void OnShopOpenned()
    {
        ItemGenerator.OnOpennedKerkere -= OnShopOpenned;
        firstTime = false;
        StartCoroutine(LogotypeFadesAway(Color.clear, 2));
    }

    IEnumerator LogotypeFadesAway(Color endValue, float duration)
    {
        float time = 0;

        while (time < duration)
        {
            logotype.color = Color.Lerp(Color.white, endValue, time / duration);
            time += Time.deltaTime;
            yield return null;
        }

        logotype.gameObject.SetActive(false);
        recordPlate.SetActive(true);
    }
}
