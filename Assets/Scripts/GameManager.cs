﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public delegate void IntDelegate(int amount);
    public static event IntDelegate OnLoseChance;

    [SerializeField] private GameObject quitPopUp;
    [SerializeField] private GameObject floorCollider;
    [SerializeField] private GameObject downCollider;
    [Header("game ending")]
    [SerializeField] private LoserGenerator loserGenerator;
    [SerializeField] private GameObject happyEnding;

    public static DragHandler currentDragItem;
    public static int currentScore;
    private static int _lostChances;
    public static int lostChances
    {
        get => _lostChances;
        set
        {
            _lostChances = value;
            OnLoseChance(lostChances);
        }
    }

    private void OnEnable()
    {
        happyEnding.SetActive(false);

        lostChances = 0;
        currentScore = 0;

        LoserGenerator.OnGameOver += OnGameOver;
        ItemGenerator.OnRestartGame += OnRestart;
        UIManager.OnFoundAllItemsOnce += GameEnding;
    }

    private void OnDisable()
    {
        LoserGenerator.OnGameOver -= OnGameOver;
        ItemGenerator.OnRestartGame -= OnRestart;
    }

    private void GameEnding()
    {
        UIManager.OnFoundAllItemsOnce -= GameEnding;
        loserGenerator.gameEnd = true;
        loserGenerator.StopBringingPeople();

        OnGameOver();

        // be happy!
        Invoke(nameof(ShowHappyEnding), 2);
    }

    private void ShowHappyEnding()
    {
        floorCollider.SetActive(true);
        downCollider.SetActive(true);
        happyEnding.SetActive(true);
    }

    private void OnGameOver()
    {
        if (currentDragItem != null)
        {
            currentDragItem.isDraging = false;
            currentDragItem.OnEndDrag();
        }
        floorCollider.SetActive(false);
        downCollider.SetActive(false);
    }

    private void OnRestart()
    {
        lostChances = 0;
        currentScore = 0;

        floorCollider.SetActive(true);
        downCollider.SetActive(true);
    }

    public void OnQuitButton()
    {
        quitPopUp.SetActive(!quitPopUp.activeInHierarchy);
    }

    public void OnQuit()
    {
        Application.Quit();
    }
}
