﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private float dragTutorialAfter;
    [SerializeField] private Transform dragTutorialCircle;
    [SerializeField] private Transform key;
    [SerializeField] private Transform lockObj;

    private bool followKey;

    private void Start()
    {
        dragTutorialCircle.gameObject.SetActive(false);
        Invoke(nameof(ShowDragTutorial), dragTutorialAfter);
    }

    private void OnEnable()
    {
        GrabWindowManager.openningShop += OnOpenedOffice;
    }

    private void OnDisable()
    {
        GrabWindowManager.openningShop -= OnOpenedOffice;
    }

    private void OnOpenedOffice()
    {
        dragTutorialCircle.gameObject.SetActive(false);
        StopAllCoroutines();
    }

    private void ShowDragTutorial()
    {
        if (ItemGenerator.isOfficeOpne)
        {
            return;
        }

        dragTutorialCircle.gameObject.SetActive(true);

        followKey = true;
        StartCoroutine(FollowKey());

        dragTutorialCircle.localScale = Vector3.zero;
        dragTutorialCircle.DOScale(Vector3.one * 2.5f, 1).SetEase(Ease.InSine).onComplete += () =>
        {
            followKey = false;
            dragTutorialCircle.DOLocalMove(lockObj.position, 0.5f).SetEase(Ease.InSine).onComplete += () =>
            {
                dragTutorialCircle.DOScale(Vector3.zero, 1).SetEase(Ease.InSine).onComplete += () =>
                {
                    Invoke(nameof(ShowDragTutorial), dragTutorialAfter);
                };
            };
        };
    }

    IEnumerator FollowKey()
    {
        while (followKey)
        {
            dragTutorialCircle.position = key.position;
            yield return new WaitForEndOfFrame();
        }
    }
}
