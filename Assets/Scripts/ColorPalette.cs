﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "palette", menuName = "color palette")]
public class ColorPalette : ScriptableObject
{
    public Color[] colors;
}
