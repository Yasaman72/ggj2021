﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class LoserGenerator : MonoBehaviour
{
    public static event genericDelegate OnGameOver;
    public static event genericDelegate OnNewGame;

    public static GameObject currentLostItem;
    [SerializeField] private Config config;
    [SerializeField] private Transform enterPos, WaitPos, LeavePose;
    [Space]
    [SerializeField] private UIManager uiManager;
    [SerializeField] private GameObject wholeLoser;
    [SerializeField] private SpriteRenderer body, head, face, lostItem, bubbleSpr;
    [SerializeField] private SpriteRenderer holdingItem;
    [SerializeField] private GameObject bubble;
    [Space]
    [SerializeField] private ColorPalette losersColorPalette;
    [SerializeField] private Sprite[] bodies;
    [SerializeField] private Sprite[] heads;
    [Space]
    [SerializeField] private Sprite angry;
    [SerializeField] private Sprite cry;
    [SerializeField] private Sprite sad;
    [SerializeField] private Sprite getting;
    [SerializeField] private Sprite ghor;
    [SerializeField] private Sprite happy;
    [SerializeField] private Sprite mostasal;
    [Header("Audio")]
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip[] worriedSounds;
    [SerializeField] private AudioClip[] sadSounds;
    [SerializeField] private AudioClip[] ghorSounds;
    [SerializeField] private AudioClip[] happySounds;
    [SerializeField] private AudioClip[] grabSounds;
    [SerializeField] private AudioClip[] crySounds;

    private Color customerColor;
    private Color bubbleOriginalColor;
    private Color lostItemColor;
    private int shakeTweenId;
    private bool customerIsWaiting;
    private bool fadeColor;
    private Sprite lastFaceSpr;
    public bool gameEnd;

    private void Start()
    {
        lostItemColor = lostItem.color;
        bubbleOriginalColor = bubbleSpr.color;
        bubble.SetActive(false);
        wholeLoser.transform.position = enterPos.position;
        ItemGenerator.OnOpennedKerkere += MakeNewLoser;
        GrabWindowManager.itemOnWindowStateChanged += ItemOnWindowStateChanged;
    }

    private void OnDisable()
    {
        ItemGenerator.OnOpennedKerkere -= MakeNewLoser;
        GrabWindowManager.itemOnWindowStateChanged -= ItemOnWindowStateChanged;
    }

    [ContextMenu("Next Loser")]
    private void MakeNewLoser()
    {
        StopAllCoroutines();
        StartCoroutine(BringNewLoser());
    }

    IEnumerator BringNewLoser()
    {
        yield return new WaitForSeconds(Random.Range(config.waitBeforeNewLoser.x, config.waitBeforeNewLoser.y));
        wholeLoser.transform.position = enterPos.position;
        holdingItem.gameObject.SetActive(false);

        customerIsWaiting = true;
        StartCoroutine(LoserWaitLoop());

        CreateNewPerson();
    }

    private void CreateNewPerson()
    {
        if (gameEnd) return;

        audioSource.PlayOneShot(worriedSounds[Random.Range(0, worriedSounds.Length)]);
        face.sprite = mostasal;

        bubble.SetActive(false);
        wholeLoser.transform.position = enterPos.position;
        wholeLoser.transform.DOShakeRotation(2, 50);
        wholeLoser.transform.DOMove(WaitPos.position, 2).onComplete += () =>
        {
            ItemGenerator.OnGaveObjectBack += OnGotObject;
            bubble.SetActive(true);
            SelectRequest();
        };

        customerColor = losersColorPalette.colors[Random.Range(0, losersColorPalette.colors.Length)];

        ResetColors();

        body.sprite = bodies[Random.Range(0, bodies.Length)];
        head.sprite = heads[Random.Range(0, heads.Length)];
    }

    private void SelectRequest()
    {
        currentLostItem = ItemGenerator.availableObjects[Random.Range(0, ItemGenerator.availableObjects.Count)];
        Sprite textSpr = currentLostItem.GetComponent<LostItem>().textSpr;

        if (textSpr != null)
        {
            lostItem.sprite = textSpr;
        }
        else
        {
            lostItem.sprite = currentLostItem.GetComponent<SpriteRenderer>().sprite;
        }

        holdingItem.sprite = currentLostItem.GetComponent<SpriteRenderer>().sprite;
    }

    private void ResetColors()
    {
        body.color = customerColor;
        head.color = customerColor;
        face.color = customerColor;
        bubbleSpr.color = bubbleOriginalColor;
        lostItem.color = lostItemColor;
    }

    IEnumerator LoserWaitLoop()
    {
        yield return new WaitForSeconds(config.timeBeforeMostasal1);
        if (customerIsWaiting)
        {
            audioSource.PlayOneShot(sadSounds[Random.Range(0, sadSounds.Length)]);
            face.sprite = sad;
        }

        yield return new WaitForSeconds(config.timeBeforeGhor2);
        if (customerIsWaiting)
        {
            audioSource.PlayOneShot(ghorSounds[Random.Range(0, ghorSounds.Length)]);
            face.sprite = ghor;
        }

        yield return new WaitForSeconds(config.timeBeforeAnger3 / 2);
        if (customerIsWaiting)
        {
            audioSource.PlayOneShot(crySounds[Random.Range(0, crySounds.Length)]);
            face.sprite = cry;
            fadeColor = true;
            StartCoroutine(CustomerFadesAway(Color.clear, config.timeBeforeAnger3 / 2));
            shakeTweenId = wholeLoser.transform.DOShakePosition(config.timeBeforeAnger3 / 2).intId;
        }
    }

    IEnumerator CustomerFadesAway(Color endValue, float duration)
    {
        float time = 0;

        while (time < duration && fadeColor)
        {
            body.color = Color.Lerp(customerColor, endValue, time / duration);
            head.color = Color.Lerp(customerColor, endValue, time / duration);
            face.color = Color.Lerp(customerColor, endValue, time / duration);
            bubbleSpr.color = Color.Lerp(bubbleOriginalColor, endValue, time / duration);
            lostItem.color = Color.Lerp(lostItemColor, endValue, time / duration);
            time += Time.deltaTime;
            yield return null;
        }

        ItemGenerator.OnGaveObjectBack -= OnGotObject;

        if (fadeColor)
        {
            body.color = endValue;
            head.color = endValue;
            face.color = endValue;
            bubbleSpr.color = endValue;
            lostItem.color = endValue;

            yield return new WaitForEndOfFrame();
            CustomerLeavingAngry();
        }
    }

    private void CustomerLeavingAngry()
    {
        MakeNewLoser();
        GameManager.lostChances++;
        CheckGameOver();
    }

    private void CheckGameOver()
    {
        if (GameManager.lostChances >= config.loseChance)
        {
            StopAllCoroutines();
            OnGameOver?.Invoke();
        }
    }


    public void StopBringingPeople()
    {
        gameEnd = true;
        StopAllCoroutines();
    }

    private void OnGotObject()
    {
        GameManager.currentScore++;
        ItemGenerator.OnGaveObjectBack -= OnGotObject;

        uiManager.OnGaveNewItem(currentLostItem);
        holdingItem.gameObject.SetActive(true);

        fadeColor = false;
        customerIsWaiting = false;

        ResetColors();

        DOTween.Kill(shakeTweenId);

        audioSource.PlayOneShot(happySounds[Random.Range(0, happySounds.Length)]);
        face.sprite = happy;
        bubble.SetActive(false);
        currentLostItem = null;

        wholeLoser.transform.DOShakeRotation(2, 50);
        wholeLoser.transform.DOMove(LeavePose.position, 1).SetDelay(1).onComplete += () =>
            {
                if (!gameEnd)
                    MakeNewLoser();
            };
    }

    private void ItemOnWindowStateChanged(bool isOnWindow)
    {
        if (!customerIsWaiting) return;

        if (isOnWindow)
        {
            audioSource.PlayOneShot(grabSounds[Random.Range(0, grabSounds.Length)]);
            lastFaceSpr = face.sprite;
            face.sprite = getting;
        }
        else
        {
            if (lastFaceSpr != null)
                face.sprite = lastFaceSpr;
        }
    }
}

