﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Collections;

public delegate void genericDelegate();

public class ItemGenerator : MonoBehaviour
{
    public static event genericDelegate OnOpennedKerkere;
    public static event genericDelegate OnClosedKerkere;
    public static event genericDelegate OnGaveObjectBack;
    public static event genericDelegate OnRestartGame;

    [SerializeField] private GameObject keyItem;
    [SerializeField] private Config config;
    [SerializeField] private ItemsDB itemsDB;
    [Space]
    [SerializeField] private GameObject kerkere;
    [SerializeField] private Transform kerkereEndPos;
    [SerializeField] private Transform kerkereStartPos;
    [SerializeField] private SpriteRenderer lockSprRen;
    [SerializeField] private Sprite openedLock;
    [SerializeField] private Sprite closedLock;
    [SerializeField] private AudioClip kerkereClose;
    [SerializeField] private AudioClip kerkereOpen;
    [SerializeField] private AudioSource audioSource;
    [Space]
    [SerializeField] private Transform itemsHolder;
    [SerializeField] private Transform leftBound, RightBound;

    [Space]
    [SerializeField] private GameObject windowCollider;

    public static List<GameObject> availableObjects = new List<GameObject>();
    public static List<GameObject> objectPool = new List<GameObject>();
    public static bool isOfficeOpne;


    private void Start()
    {
        // create the pool

        foreach (GameObject item in itemsDB.lostItems)
        {
            var newObj = Instantiate(item, itemsHolder);
            newObj.transform.localScale = newObj.transform.localScale * Random.Range(config.scaleRandomRange.x, config.scaleRandomRange.y);
            newObj.GetComponent<Rigidbody2D>().gravityScale = Random.Range(config.gravityRandomRange.x, config.gravityRandomRange.y);
            newObj.SetActive(false);
            objectPool.Add(newObj);
        }

        for (int i = 0; i < config.maxItemInScreen - itemsDB.lostItems.Length; i++)
        {
            var newObj = Instantiate(itemsDB.lostItems[Random.Range(0, itemsDB.lostItems.Length)], itemsHolder);
            newObj.transform.localScale = newObj.transform.localScale * Random.Range(config.scaleRandomRange.x, config.scaleRandomRange.y);
            newObj.GetComponent<Rigidbody2D>().gravityScale = Random.Range(config.gravityRandomRange.x, config.gravityRandomRange.y);
            newObj.SetActive(false);
            objectPool.Add(newObj);
        }
    }

    private void OnEnable()
    {
        windowCollider.SetActive(true);
        LoserGenerator.OnGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        LoserGenerator.OnGameOver -= OnGameOver;
    }

    public void OnOpenedOffice()
    {

        isOfficeOpne = true;
        windowCollider.SetActive(false);

        lockSprRen.sprite = openedLock;

        audioSource.PlayOneShot(kerkereOpen);
        kerkere.transform.DOMove(kerkereEndPos.position, 1).SetEase(Ease.InOutSine).onComplete += () =>
            {
                for (int i = 0; i < config.initialGameItemsCound; i++)
                {
                    GenerateItem();
                }

                StartCoroutine(NewItemGenerator());

                Invoke(nameof(EnableWindowCollider), 2);

                OnOpennedKerkere?.Invoke();
            };
    }

    private void EnableWindowCollider()
    {
        windowCollider.SetActive(true);
    }

    IEnumerator NewItemGenerator()
    {
        while (Application.isPlaying)
        {
            yield return new WaitForSeconds(Random.Range(config.newItemInterval.x, config.newItemInterval.y));

            if (config.maxItemInScreen > availableObjects.Count)
                GenerateItem();
        }
    }

    private void GenerateItem()
    {
        var newObj = objectPool[Random.Range(0, objectPool.Count)];
        newObj.transform.position = new Vector2(Random.Range(leftBound.position.x, RightBound.position.x), RightBound.position.y);
        newObj.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
        newObj.SetActive(true);
        objectPool.Remove(newObj);
        availableObjects.Add(newObj);
    }

    public static void OnGaveAnObjedctBack(GameObject obj)
    {
        RemoveObjectFromList(obj);
        OnGaveObjectBack?.Invoke();
    }

    public static void RemoveObjectFromList(GameObject obj)
    {
        if (!availableObjects.Contains(obj)) return;
        obj.SetActive(false);
        objectPool.Add(obj);
        availableObjects.Remove(obj);
    }

    private void OnGameOver()
    {
        windowCollider.SetActive(true);

        StopAllCoroutines();
        Invoke(nameof(RestartGame), config.restartGameAfter);
    }

    private void RestartGame()
    {
        OnRestartGame?.Invoke();

        lockSprRen.sprite = closedLock;

        audioSource.PlayOneShot(kerkereClose);
        kerkere.transform.DOMove(kerkereStartPos.position, 1).SetEase(Ease.InOutSine).onComplete += () =>
        {
            //generate the key
            Vector2 pos = new Vector2(Random.Range(leftBound.position.x, RightBound.position.x), RightBound.position.y);
            Quaternion rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            var newObj = Instantiate(keyItem, pos, rotation, itemsHolder);
            newObj.transform.localScale = newObj.transform.localScale * Random.Range(config.scaleRandomRange.x, config.scaleRandomRange.y);
            newObj.GetComponent<Rigidbody2D>().gravityScale = Random.Range(config.gravityRandomRange.x, config.gravityRandomRange.y);
            availableObjects.Add(newObj);
            OnClosedKerkere?.Invoke();
        };
    }
}