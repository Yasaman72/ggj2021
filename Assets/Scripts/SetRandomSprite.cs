﻿using UnityEngine;
using UnityEngine.UI;

public class SetRandomSprite : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private Sprite[] sprites;

    void Start()
    {
        image.sprite = sprites[Random.Range(0, sprites.Length)];
    }
}
