﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    [SerializeField] private AudioMixerGroup ghorMixer;
    [SerializeField] private List<AudioClip> ghorClips;
    [SerializeField] private int ghorSourcesNumber;
    [SerializeField] private Vector2 ghorNumberRange;
    [SerializeField] private float delayBetweenGhors;
    private List<AudioSource> ghorSources;
    private int ghorSourceIndex = 0;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        ghorSources = new List<AudioSource>();
        CreateGhorSources();
    }

    void Update()
    {
        
    }

    public void PlayGhorsounds(bool isManual = false)
    {
        int rnd;
        if (isManual)
        {
            rnd = 1;
        }
        else
        {
            rnd = (int)Random.Range(ghorNumberRange.x, ghorNumberRange.y + 1);
        }

        for (int i = 0; i < rnd; i++)
        {
            ghorSources[ghorSourceIndex].clip = ghorClips[Random.Range(0, ghorClips.Count - 1)];
            ghorSources[ghorSourceIndex].pitch = Random.Range(0.9f, 1.1f);
            ghorSources[ghorSourceIndex].panStereo = (Random.Range(0, 2) - 1) * Random.Range(0.1f, 0.3f);
            ghorSources[ghorSourceIndex].PlayDelayed((ulong)(delayBetweenGhors * i * 44100));
            if (ghorSourceIndex == ghorSources.Count - 1)
            {
                ghorSourceIndex = 0;
            }
            else
            {
                ghorSourceIndex++;
            }
        }
    }

    private void CreateGhorSources()
    {
        for (int i = 0; i < ghorSourcesNumber; i++)
        {
            AudioSource newSource = transform.gameObject.AddComponent<AudioSource>();
            newSource.playOnAwake = false;
            newSource.outputAudioMixerGroup = ghorMixer;
            ghorSources.Add(newSource);
        }
    }
}
