﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Items Data Base", menuName = "Items DB")]
public class ItemsDB : ScriptableObject
{
    public GameObject[] lostItems;
}
